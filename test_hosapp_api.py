# -*- coding: utf-8 -*-
import requests
import json
import unittest
import urllib3
import json


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class TestHosAppApi(unittest.TestCase):
    def test_sprint_1(self):
        print("\n")
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        if datatest["data"][0]["sprint"] == "1":
            for idx_service in range(len(datatest["data"][0]["services"])):
                service = datatest["data"][0]["services"][idx_service]["service"]
                api_url = datatest["data"][0]["services"][idx_service]["api"]
                body = datatest["data"][0]["services"][idx_service]["body"]

                if service != "token":
                    res = requests.post(api_url, data=json.dumps(
                        body), verify=False, headers=header_without_token)

                    messageCode = json.loads(res.text)["messageCode"]
                    if messageCode == 10000:
                        print("Service: " + service + " ==> " +
                              bcolors.OKGREEN + "[SUCCESS]" + bcolors.ENDC)
                        # print("Response: " + json.dumps(response, ensure_ascii=False))
                    else:
                        print("Service: " + service + " ==> " +
                              bcolors.FAIL + "[FAIL]" + bcolors.ENDC)
                        print("Response: " + res.text)
                        # print("Response: " + json.dumps(response, ensure_ascii=False))

                # # Test verify4SignUp
                # if service == "verify4Signup":
                #     res = requests.post(api_url, data=json.dumps(body),
                #                         verify=False, headers=header_without_token)
                #     # response = res.json()
                #     # print(response)
                #     if res.status_code == 200:
                #         print("Service: " + service + " ==> " + "[SUCCESS]")
                #         # print("Response: " + json.dumps(response, ensure_ascii=False))
                #     else:
                #         print("Service: " + service + " ==> " + "[FAIL]")
                #         # print("Response: " + json.dumps(response, ensure_ascii=False))

                # # Test patientHosAppSignup
                # elif service == "patientHosAppSignup":
                #     res = requests.post(api_url, data=json.dumps(body),
                #                         verify=False, headers=header_without_token)
                #     # response = res.json()
                #     # print(response)
                #     if res.status_code == 200:
                #         print("Service: " + service + " ==> " + "[SUCCESS]")
                #         # print("Response: " + json.dumps(response, ensure_ascii=False))
                #     else:
                #         print("Service: " + service + " ==> " + "[FAIL]")
                #         # print("Response: " + json.dumps(response, ensure_ascii=False))

    def test_sprint_2(self):
        print("\n")
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        if datatest["data"][1]["sprint"] == "2":
            for idx_service in range(len(datatest["data"][1]["services"])):
                service = datatest["data"][1]["services"][idx_service]["service"]
                api_url = datatest["data"][1]["services"][idx_service]["api"]
                body = datatest["data"][1]["services"][idx_service]["body"]

                res = requests.post(api_url, data=json.dumps(
                    body), verify=False, headers=header_with_token)
                # response = res.json()
                # print(response)
                messageCode = json.loads(res.text)["messageCode"]
                if messageCode == 10000:
                    print("Service: " + service + " ==> " +
                          bcolors.OKGREEN + "[SUCCESS]" + bcolors.ENDC)
                    # print("Response: " + json.dumps(response, ensure_ascii=False))
                else:
                    print("Service: " + service + " ==> " +
                          bcolors.FAIL + "[FAIL]" + bcolors.ENDC)
                    print("Response: " + res.text)
                    # print("Response: " + json.dumps(response, ensure_ascii=False))

                # # Test patientProfile
                # if service == "patientProfile":
                #     res = requests.post(api_url, data=json.dumps(body),
                #                         verify=False, headers=header_with_token)
                #     # response = res.json()
                #     # print(response)
                #     if res.status_code == 200:
                #         print("Service: " + service + " ==> " + "[SUCCESS]")
                #         # print("Response: " + json.dumps(response, ensure_ascii=False))
                #     else:
                #         print("Service: " + service + " ==> " + "[FAIL]")
                #         # print("Response: " + json.dumps(response, ensure_ascii=False))

                # # Test medicalWelfares
                # elif service == "medicalWelfares":
                #     res = requests.post(api_url, data=json.dumps(body),
                #                         verify=False, headers=header_with_token)
                #     # response = res.json()
                #     # print(response)
                #     if res.status_code == 200:
                #         print("Service: " + service + " ==> " + "[SUCCESS]")
                #         # print("Response: " + json.dumps(response, ensure_ascii=False))
                #     else:
                #         print("Service: " + service + " ==> " + "[FAIL]")
                #         # print("Response: " + json.dumps(response, ensure_ascii=False))

                # # Test sendAppointmentNotification
                # elif service == "sendAppointmentNotification":
                #     res = requests.post(api_url, data=json.dumps(body),
                #                         verify=False, headers=header_with_token)
                #     # response = res.json()
                #     # print(response)
                #     if res.status_code == 200:
                #         print("Service: " + service + " ==> " + "[SUCCESS]")
                #         # print("Response: " + json.dumps(response, ensure_ascii=False))
                #     else:
                #         print("Service: " + service + " ==> " + "[FAIL]")
                #         # print("Response: " + json.dumps(response, ensure_ascii=False))

    def test_sprint_3(self):
        print("\n")
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        if datatest["data"][2]["sprint"] == "3":
            for idx_service in range(len(datatest["data"][2]["services"])):
                service = datatest["data"][2]["services"][idx_service]["service"]
                api_url = datatest["data"][2]["services"][idx_service]["api"]
                body = datatest["data"][2]["services"][idx_service]["body"]

                # print(service)
                # print(api_url)
                # print(body)
                res = requests.post(api_url, data=json.dumps(
                    body), verify=False, headers=header_with_token)
                # response = res.json()
                # print(json.loads(res.text)["messageCode"])

                messageCode = json.loads(res.text)["messageCode"]
                if messageCode == 10000:
                    print("Service: " + service + " ==> " +
                          bcolors.OKGREEN + "[SUCCESS]" + bcolors.ENDC)
                    # print("Response: " + json.dumps(response, ensure_ascii=False))
                else:
                    print("Service: " + service + " ==> " +
                          bcolors.FAIL + "[FAIL]" + bcolors.ENDC)
                    print("Response: " + res.text)

    def test_sprint_4(self):
        print("\n")
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        if datatest["data"][3]["sprint"] == "4":
            for idx_service in range(len(datatest["data"][3]["services"])):
                service = datatest["data"][3]["services"][idx_service]["service"]
                api_url = datatest["data"][3]["services"][idx_service]["api"]
                body = datatest["data"][3]["services"][idx_service]["body"]

                result = requests.post(api_url, data=json.dumps(
                    body), verify=False, headers=header_with_token)
                # response = res.json()
                # print(result.text)
                messageCode = json.loads(res.text)["messageCode"]
                if messageCode == 10000:
                    print("Service: " + service + " ==> " +
                          bcolors.OKGREEN + "[SUCCESS]" + bcolors.ENDC)
                    # print("Response: " + json.dumps(response, ensure_ascii=False))
                else:
                    print("Service: " + service + " ==> " +
                          bcolors.FAIL + "[FAIL]" + bcolors.ENDC)
                    print("Response: " + res.text)
                    # print("Response: " + json.dumps(response, ensure_ascii=False))

    def test_sprint_5(self):
        print("\n")
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        if datatest["data"][4]["sprint"] == "5":
            for idx_service in range(len(datatest["data"][4]["services"])):
                service = datatest["data"][4]["services"][idx_service]["service"]
                api_url = datatest["data"][4]["services"][idx_service]["api"]
                body = datatest["data"][4]["services"][idx_service]["body"]

                res = requests.post(api_url, data=json.dumps(
                    body), verify=False, headers=header_with_token)
                # response = res.json()
                # print(response)
                messageCode = json.loads(res.text)["messageCode"]
                if messageCode == 10000:
                    print("Service: " + service + " ==> " +
                          bcolors.OKGREEN + "[SUCCESS]" + bcolors.ENDC)
                    # print("Response: " + json.dumps(response, ensure_ascii=False))
                else:
                    print("Service: " + service + " ==> " +
                          bcolors.FAIL + "[FAIL]" + bcolors.ENDC)
                    print("Response: " + res.text)
                    # print("Response: " + json.dumps(response, ensure_ascii=False))


if __name__ == '__main__':
    with open('datatest.json', encoding='utf-8') as f:
        datatest = json.load(f)

    service_name = datatest["data"][0]["services"][0]["service"]
    api_url = datatest["data"][0]["services"][0]["api"]
    body = datatest["data"][0]["services"][0]["body"]

    header_without_token = {
        "Content-Type": "application/json"
    }

    header_with_token = ""

    # if service_name == "token":
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    res = requests.post(api_url, data=json.dumps(body),
                        verify=False, headers=header_without_token)
    response = res.json()
    token = response["accessToken"]

    if service_name == "token":
        header_with_token = {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        }
        print(header_with_token)

    unittest.main()
